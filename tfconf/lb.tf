resource "aws_lb" "d4auth" {
  name               = "d4auth"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["${aws_subnet.private.id}", "${aws_subnet.niquemouque.id}"]
}

resource "aws_lb_listener" "front_end" {
  depends_on        = ["aws_acm_certificate_validation.cert"]
  load_balancer_arn = "${aws_lb.d4auth.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${aws_acm_certificate.cert.arn}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.d4auth.arn}"
  }
}

resource "aws_lb_target_group" "d4auth" {
  name        = "d4auth"
  port        = 8080
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${aws_vpc.d4auth.id}"

  health_check {
    interval = 10
    path     = "/health"
    port     = 8080
    protocol = "HTTP"
    matcher  = "200"
  }
}

resource "aws_lb_listener" "front_end_http" {
  load_balancer_arn = "${aws_lb.d4auth.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
