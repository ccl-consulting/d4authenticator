resource "aws_vpc" "d4auth" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.d4auth.id}"
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "d4auth_public"
  }
}

resource "aws_subnet" "niquemouque" {
  vpc_id            = "${aws_vpc.d4auth.id}"
  availability_zone = "eu-west-1b"
  cidr_block        = "10.0.2.0/24"
}

resource "aws_subnet" "private" {
  vpc_id     = "${aws_vpc.d4auth.id}"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "d4auth_private"
  }
}

resource "aws_internet_gateway" "public" {
  vpc_id = "${aws_vpc.d4auth.id}"
}

resource "aws_eip" "nat" {}

resource "aws_nat_gateway" "gw" {
  depends_on = ["aws_internet_gateway.public"]

  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public.id}"
}

resource "aws_network_interface" "bastion" {
  private_ips = ["10.0.0.99"]
  subnet_id   = "${aws_subnet.public.id}"

  security_groups = ["${aws_security_group.bastion.id}"]
}

resource "aws_eip" "bastion" {
  vpc = true

  associate_with_private_ip = "10.0.0.99"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.d4auth.id}"
}

resource "aws_route" "outbound_to_ig" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.public.id}"
}

resource "aws_main_route_table_association" "public" {
  vpc_id         = "${aws_vpc.d4auth.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.d4auth.id}"
}

resource "aws_route" "private_outbound_nat_gw" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.gw.id}"
}

resource "aws_route_table_association" "d4auth" {
  subnet_id      = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.private.id}"
}
