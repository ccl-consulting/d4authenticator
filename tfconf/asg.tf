resource "aws_launch_configuration" "d4auth" {
  name          = "authenticator"
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"

  key_name        = "d4auth_installer"
  security_groups = ["${aws_security_group.d4auth_external.id}"]
  user_data       = "${file("./cloud-init.yml")}"

  lifecycle {
    create_before_destroy = true
  }
}

# resource "aws_autoscaling_policy" "d4auth_rps" {
#   name                   = "d4auth_rps"
#   scaling_adjustment     = 4
#   adjustment_type        = "ChangeInCapacity"
#   cooldown               = 300
#   autoscaling_group_name = "${aws_autoscaling_group.d4auth.name}"
# }

resource "aws_autoscaling_group" "d4auth" {
  availability_zones        = ["eu-west-1a", "eu-west-1b"]
  name                      = "d4auth"
  max_size                  = 20
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.d4auth.name}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = "${aws_autoscaling_group.d4auth.id}"
  alb_target_group_arn   = "${aws_lb_target_group.d4auth.arn}"
}
