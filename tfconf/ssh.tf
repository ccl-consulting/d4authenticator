resource "aws_key_pair" "d4auth_installer" {
  key_name   = "d4auth_installer"
  public_key = "${var.public_key}"
}