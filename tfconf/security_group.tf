resource "aws_security_group" "d4auth_external" {
  name        = "d4auth_external"
  description = "Allow inbound HTTP 8080 and ICMP for everyone"
  vpc_id      = "${aws_vpc.d4auth.id}"

  ## HTTPS
  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ## ICMP
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "icmp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "bastion" {
  name        = "bastion"
  description = "Allow inbound SSH and ICMP for everyone"
  vpc_id      = "${aws_vpc.d4auth.id}"

  ## SSH
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ## ICMP
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "icmp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}