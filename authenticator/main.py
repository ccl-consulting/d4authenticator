#!/usr/bin/python3

import os
import time
import random
import string

import flask
import boto3

from functools import wraps

from ratelimit import limits, RateLimitException

app = flask.Flask(__name__)

METRICS = []

EXPORTER = boto3.client('cloudwatch')


def send_metric_to_cloudwatch():
    global EXPORTER

    EXPORTER.put_metric_data(
        Namespace='d4auth',
        MetricData=[
            dict(
                MetricName='req_rps_avg',
                Value=get_avg_rps_1m()
            )
        ]
    )


def get_avg_rps_1m():
    global METRICS

    win_stop = time.time()
    win_start = win_stop - 60
    METRICS = list(filter(lambda x: x > win_start, METRICS))
    return (len(METRICS) / 60)


@app.route("/metrics")
def metrics():
    return '{"ok": true, "avg_rps_1m": %s}' % get_avg_rps_1m(), 200


def fail_reqs_above_quota(f):
    @wraps(f)
    def __wrap__():
        global METRICS
        METRICS += [time.time()]
        try:
            return f()
        except RateLimitException:
            return flask.Response(
                '{"ok": false, "msg": "Error 37", "token": null}',
                status=500,
                mimetype='application/json'
            )
    return __wrap__


@app.route("/auth")
@fail_reqs_above_quota
@limits(calls=300, period=1)
def draw_token_from_void():
    token = ''.join(
        random.choice(string.ascii_uppercase + string.digits)
        for _ in range(16)
    )
    return flask.Response(
        '{"ok": true, "msg": "Auth success", "token": "%s"}' % token,
        status=200,
        mimetype='application/json'
    )


app.run(host='0.0.0.0', port=8081)
