DOCKER_ORG := cclorg

RM := rm -rvf
MD := mkdir -p
TFOPTS := -var-file=secrets.tfvars -var-file=domain.tfvars
HK_IGN := -o 'StrictHostKeyChecking=no' -o 'UserKnownHostsFile=/dev/null'

all: stage-0


mrproper: clean
	terraform destroy $(TFOPTS) -auto-approve -var "public_key=empty" tfconf
	$(RM) \
		terraform.tfstate \
		terraform.tfstate.* \
		tf.plan \
		.terraform

clean:
	$(RM) output

output/id_rsa:
	$(MD) output
	echo $${PWD}/output/id_rsa | ssh-keygen -t rsa -b 4096

output: \
		output/id_rsa

_tf_init:
	terraform init tfconf
	terraform get tfconf
.PHONY += _tf_init

infra: _tf_init output/id_rsa
	terraform plan $(TFOPTS) -var "public_key=$(shell cat $${PWD}/output/id_rsa.pub)" -out tf.plan tfconf
	terraform apply -auto-approve tf.plan
.PHONY += infra


stage-0: output/id_rsa infra
.PHONY += stage-0
